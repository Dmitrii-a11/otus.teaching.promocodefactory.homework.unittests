﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Http.HttpResults;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitTests : IDisposable
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsError404()
        {
            // Arrange
            var partnerId = Guid.Empty;
            Partner partner = null;
            var requestMock = new Mock<SetPartnerPromoCodeLimitRequest>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, requestMock.Object);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsError400()
        {
            // Arrange
            Mock<Partner> partnerMock = new();
            var partnerId = Guid.Empty;
            _ = partnerMock.SetupAllProperties();
            partnerMock.Object.IsActive = false;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partnerMock.Object);
            var requestMock = new Mock<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, requestMock.Object);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimits_NumberIssuedPromoCodesEqualsZero()
        {
            // Arrange
            Mock<Partner> partnerMock = new();
            _ = partnerMock.SetupAllProperties();
            partnerMock.Object.NumberIssuedPromoCodes = 1;
            partnerMock.Object.IsActive = true;
            partnerMock.Object.PartnerLimits = new List<PartnerPromoCodeLimit>() { new() };

            var partnerId = Guid.Empty;
           
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partnerMock.Object);
            var requestMock = new Mock<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, requestMock.Object);

            // Assert
            partnerMock.Object.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimits_PreviousPartnerPromoCodeLimitCancelDateEqualsToDay()
        {
            // Arrange
            Mock<Partner> partnerMock = new();
            _ = partnerMock.SetupAllProperties();
            partnerMock.Object.IsActive = true;
            partnerMock.Object.PartnerLimits = new List<PartnerPromoCodeLimit>() { new() };

            var partnerId = Guid.Empty;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partnerMock.Object);
            var requestMock = new Mock<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, requestMock.Object);

            // Assert
            partnerMock.Object.PartnerLimits.First().CancelDate.Value.Date.Should().Be(DateTime.Now.Date);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsLessThanZero_ReturnsError400()
        {
            // Arrange
            Mock<Partner> partnerMock = new();
            _ = partnerMock.SetupAllProperties();
            partnerMock.Object.IsActive = true;
            partnerMock.Object.PartnerLimits = new List<PartnerPromoCodeLimit>() { new() };

            var partnerId = Guid.Empty;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partnerMock.Object);
            var requestMock = new Mock<SetPartnerPromoCodeLimitRequest>();
            requestMock.SetupAllProperties();
            requestMock.Object.Limit = 0;

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, requestMock.Object);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        public void Dispose()
        {

        }
    }
}
﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Promocodes;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PromocodesTests : IClassFixture<TestFixture>
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly PromocodesController controller;

        public PromocodesTests(TestFixture testFixture)
        {
            _promocodeRepository = testFixture.ServiceProvider.GetRequiredService<IRepository<PromoCode>>();
            controller = new PromocodesController(_promocodeRepository);
        }

        [Fact]
        public async void GivePromoCodesToCustomersWithPreferenceAsync_()
        {
            // Assert
            var promocodeValue = Guid.NewGuid().ToString();
            var request = new GivePromoCodeRequest
            {
                PromoCode = promocodeValue
            };

            // Act
            var result = (await controller.GivePromoCodesToCustomersWithPreferenceAsync(request)) as OkObjectResult;
            var response = result.Value as PromoCodeShortResponse;
            var promoCodes = await controller.GetPromocodesAsync();
            var excpectedpromoCode = promoCodes.Value.Where(p => p.Code == promocodeValue).FirstOrDefault();

            // Assert
            response.Code.Should().BeEquivalentTo(excpectedpromoCode.Code);
        }
    }
}
